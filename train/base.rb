module Train
  class Base
    attr_accessor :file_input

    def initialize
      parse_file_input
    end

    def parse_file_input
      file_input = self.file_input || File.join(Dir.pwd, 'inputs', 'train_routes.txt')
      @inputs = File.read(file_input).split(', ').map{|i| {"#{i[0]}#{i[1]}".upcase => i[2..-1].to_i, :start => i[0], :end => i[1] }}

    rescue Errno::ENOENT => e
      puts 'Place Provide a text(.txt) file inside inputs folder and set the path to @file_input for the input with a content. e.g (AB6, BC8, CD9) as a straight line.'
      exit
    end

    protected
      def execute!(label, &expressions)
        result = "#{expressions.call}"
        raise Train::RouteNotFound if result == '0'
        puts "#{label}: #{result}"
        result
      rescue Train::RouteNotFound => e
        error = "#{label}: #{e.message}"
        puts error
        error
      rescue StandardError
        error = "#{label}: #{Train::RouteNotFound.new.message}"
        puts error
        error
      end

      def compute_total_distance(label, route)
        combinations = route_combinations(route)
        execute!(label){combinations.inject(0){|sum, route| sum + retrieve_distance(route)}}
      end

      def route_combinations(routes)
        towns = routes.split('-')
        towns.map.with_index{|v,i| v + towns[i + 1] if towns[i + 1]}.compact unless routes.empty?
      end

      def retrieve_distance(route)
        if distance = @inputs.find{|input| input.keys.first.match route}
          return distance.values.first.to_i
        end

        raise RouteNotFound
      end
  end
end