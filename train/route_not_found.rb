class Train::RouteNotFound < StandardError
  def message
    'NO SUCH ROUTE'
  end
end