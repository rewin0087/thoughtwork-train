require File.join(Dir.pwd, 'train', 'base')
require File.join(Dir.pwd, 'train', 'route_not_found')

module Train
  class Trip < Train::Base
    def run_all
      # 6. The number of trips starting at C and ending at C with a maximum of 3 stops.
      trip_for_c_to_c_with_max_of_3_stops
      # 7. The number of trips starting at A and ending at C with exactly 4 stops.
      trip_for_a_to_c_with_max_of_4_stop
      # 8. The length of the shortest route (in terms of distance to travel) from A to C.
      shortest_route_for_a_to_c
      # 9. The length of the shortest route (in terms of distance to travel) from B to B.
      shortest_route_for_b_to_b
      # 10. The number of different routes from C to C with a distance of less than 30.
      different_routes_from_c_to_c_with_a_distance_less_than_30
    end

    def trip_for_c_to_c_with_max_of_3_stops
      start_route, end_route, max_stop = 'C', 'C', 3
      trip = routes_start_with(start_route).map{|route| compute_total_number_of_trip(route, end_route, 0){|total| total > 1 && total <= max_stop}}.flatten
      execute!('Question #6. The number of trips starting at C and ending at C with a maximum of 3 stops'){ trip.size }
    end

    def trip_for_a_to_c_with_max_of_4_stop
      start_route, end_route, max_stop = 'A', 'C', 4
      trip = routes_start_with(start_route).map{|route| compute_total_number_of_trip(route, end_route, 0){|total| total == max_stop}}.flatten
      execute!('Question #7. The number of trips starting at A and ending at C with exactly 4 stops'){ trip.size }
    end

    def shortest_route_for_a_to_c
      start_route, end_route = 'A', 'C'
      distances = routes_start_with(start_route).map{|route| compute_total_distance_to_travel(route, end_route, 0)}.flatten
      execute!('Question #8. The length of the shortest route (in terms of distance to travel) from A to C'){ distances.min }
    end

    def shortest_route_for_b_to_b
      start_route, end_route = 'B', 'B'
      distances = routes_start_with(start_route).map{|route| compute_total_distance_to_travel(route, end_route, 0)}.flatten
      execute!('Question #9. The length of the shortest route (in terms of distance to travel) from B to B'){ distances.min }
    end

    def different_routes_from_c_to_c_with_a_distance_less_than_30
      start_route, end_route, max_distance = 'C', 'C', 30
      trips = routes_start_with(start_route).map{|route| different_routes_with_a_distance_less_than_of(route, end_route, [], '', max_distance)}.flatten.uniq
      execute!('Question #10. The number of different routes from C to C with a distance of less than 30'){ trips.size }
    end

    private
      def different_routes_with_a_distance_less_than_of(town, distination, routes, route, max)
        route = (route + town.keys.first).squeeze # removed consecutive occurence of character
        distance = compute_total_distance!(route)
        next_routes = routes_start_with town[:end]
        routes << {route: route, distance: distance, town: town} if town[:end] == distination && distance < max && !routes.include?(route)
        return routes if distance > max
        next_routes.map{|town| different_routes_with_a_distance_less_than_of(town, distination, routes, route, max)}
      end

      def compute_total_number_of_trip(town, distination, total, &max)
        total+=1
        next_routes = routes_start_with town[:end]
        return total if town[:end] == distination && max.call(total) # call block
        next_routes.map{|route| compute_total_number_of_trip(route, distination, total){|total| return max.call(total)}}
      end

      def compute_total_distance_to_travel(town, distination, total)
        total+=town.values.first
        next_routes = routes_start_with town[:end]
        return total if town[:end] == distination || reversable_route?(next_routes, town)
        next_routes.map{|route| compute_total_distance_to_travel(route, distination, total)}
      end

      def reversable_route?(next_routes, town)
        next_routes.find{|r| r.keys.first == town.keys.first.reverse}
      end

      def routes_start_with(town)
        @inputs.select{|input| input[:start] == town }
      end

      def compute_total_distance!(route)
        combinations = route_combinations(route)
        combinations.inject(0){|sum, route| sum + retrieve_distance(route)} unless route.empty?
      end

      def route_combinations(routes)
        towns = routes.split(//)
        towns.map.with_index{|v,i| v + towns[i + 1] if towns[i + 1]}.compact unless routes.empty?
      end
  end
end