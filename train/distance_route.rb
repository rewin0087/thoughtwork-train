require File.join(Dir.pwd, 'train', 'base')
require File.join(Dir.pwd, 'train', 'route_not_found')

module Train
  class DistanceRoute < Train::Base
    def run_all
      # 1. The distance of the route A-B-C.
      route_abc
      # 2. The distance of the route A-D.
      route_ad
      # 3. The distance of the route A-D-C.
      route_adc
      # 4. The distance of the route A-E-B-C-D.
      route_aebcd
      # 5. The distance of the route A-E-D.
      route_aed
    end

    def route_abc
      compute_total_distance('Question #1. The distance of the route A-B-C', 'A-B-C')
    end

    def route_ad
      compute_total_distance('Question #2. The distance of the route A-D', 'A-D')
    end

    def route_adc
      compute_total_distance('Question #3. The distance of the route A-D-C', 'A-D-C')
    end

    def route_aebcd
      compute_total_distance('Question #4. The distance of the route A-E-B-C-D', 'A-E-B-C-D')
    end

    def route_aed
      compute_total_distance('Question #5. The distance of the route A-E-D', 'A-E-D')
    end
  end
end