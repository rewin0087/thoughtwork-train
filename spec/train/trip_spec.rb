require File.join(Dir.pwd,'train', 'trip')

RSpec.describe Train::Trip do
  let!(:trip) { Train::Trip.new }

  describe 'default input' do
    context '#trip_for_c_to_c_with_max_of_3_stops' do
      it { expect(trip.trip_for_c_to_c_with_max_of_3_stops).to eql('2') }
    end

    context '#trip_for_a_to_c_with_max_of_4_stop' do
      it { expect(trip.trip_for_a_to_c_with_max_of_4_stop).to eql('3') }
    end

    context '#shortest_route_for_a_to_c' do
      it { expect(trip.shortest_route_for_a_to_c).to eql('9') }
    end

    context '#shortest_route_for_b_to_b' do
      it { expect(trip.shortest_route_for_b_to_b).to eql('9') }
    end

    context '#different_routes_from_c_to_c_with_a_distance_less_than_30' do
      it { expect(trip.different_routes_from_c_to_c_with_a_distance_less_than_30).to eql('7') }
    end
  end

  describe 'where inputs are AB1, BC4, CD6, DC4, DE2, AD2, CE3, EB1' do
    let(:file_input) { File.join(Dir.pwd, 'spec', 'fixtures', 'train_routes.txt') }

    before do
      trip.file_input = file_input
      trip.parse_file_input
    end

    context '#trip_for_c_to_c_with_max_of_3_stops' do
      it { expect(trip.trip_for_c_to_c_with_max_of_3_stops).to eql('2') }
    end

    context '#trip_for_a_to_c_with_max_of_4_stop' do
      it { expect(trip.trip_for_a_to_c_with_max_of_4_stop).to eql('2') }
    end

    context '#shortest_route_for_a_to_c' do
      it { expect(trip.shortest_route_for_a_to_c).to eql('5') }
    end

    context '#shortest_route_for_b_to_b' do
      it { expect(trip.shortest_route_for_b_to_b).to eql('8') }
    end

    context '#different_routes_from_c_to_c_with_a_distance_less_than_30' do
      it { expect(trip.different_routes_from_c_to_c_with_a_distance_less_than_30).to include('22') }
    end
  end
end