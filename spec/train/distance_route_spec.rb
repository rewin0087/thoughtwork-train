require File.join(Dir.pwd,'train', 'distance_route')

RSpec.describe Train::DistanceRoute do
  let!(:distance_route) { Train::DistanceRoute.new }

  describe 'default input' do
    context '#route_abc' do
      it { expect(distance_route.route_abc).to eql('9') }
    end

    context '#route_ad' do
      it { expect(distance_route.route_ad).to eql('5') }
    end

    context '#route_adc' do
      it { expect(distance_route.route_adc).to eql('13') }
    end

    context '#route_aebcd' do
      it { expect(distance_route.route_aebcd).to eql('22') }
    end

    context '#route_aed' do
      it { expect(distance_route.route_aed).to include('NO SUCH ROUTE') }
    end
  end

  describe 'where inputs are AB1, BC4, CD6, DC4, DE2, AD2, CE3, EB1' do
    let(:file_input) { File.join(Dir.pwd, 'spec', 'fixtures', 'train_routes.txt') }

    before do
      distance_route.file_input = file_input
      distance_route.parse_file_input
    end

    context '#route_abc' do
      it { expect(distance_route.route_abc).to eql('5') }
    end

    context '#route_ad' do
      it { expect(distance_route.route_ad).to eql('2') }
    end

    context '#route_adc' do
      it { expect(distance_route.route_adc).to eql('6') }
    end

    context '#route_aebcd' do
      it { expect(distance_route.route_aebcd).to include('NO SUCH ROUTE') }
    end

    context '#route_aed' do
      it { expect(distance_route.route_aed).to include('NO SUCH ROUTE') }
    end
  end
end