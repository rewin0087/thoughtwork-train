require File.join(Dir.pwd, 'train', 'distance_route')
require File.join(Dir.pwd, 'train', 'trip')

Train::DistanceRoute.new.run_all
Train::Trip.new.run_all